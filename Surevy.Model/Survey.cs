﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Survey.Model
{
    public class SurveyEntity
    {
        public int ID { get; set; }
        [Range(1, 5)]
        public int Quality { get; set; }
        [Range(1, 5)]
        public int Expertise { get; set; }
        [Range(1, 5)]
        public int Culture { get; set; }
        
        public double Average
        {
            get
            { return (double)(Quality + Expertise + Culture) / 3; }
        }

        public string Comments { get; set; }

        public string Sentiment { get; set; }

    }
}
