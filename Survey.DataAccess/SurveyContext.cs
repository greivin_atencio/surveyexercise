﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using Survey.Model;


namespace Survey.DataAccess
{
    public class SurveyContext : DbContext
    {
        public SurveyContext() : base("SurveyDB") { }

        public DbSet<SurveyEntity> Surveys { get; set; }
    }
}
