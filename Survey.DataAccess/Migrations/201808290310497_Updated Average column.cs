namespace Survey.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatedAveragecolumn : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.SurveyEntities", "Average");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SurveyEntities", "Average", c => c.Double(nullable: false));
        }
    }
}
