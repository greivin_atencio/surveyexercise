namespace Survey.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addedaveragevalue : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SurveyEntities", "Average", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SurveyEntities", "Average");
        }
    }
}
