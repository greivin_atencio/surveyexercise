namespace Survey.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initialsetup : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SurveyEntities",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Quality = c.Int(nullable: false),
                        Expertise = c.Int(nullable: false),
                        Culture = c.Int(nullable: false),
                        Comments = c.String(),
                        Sentiment = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.SurveyEntities");
        }
    }
}
