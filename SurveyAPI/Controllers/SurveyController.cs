﻿using Survey.Model;
using SurveyAPI.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SurveyAPI.Controllers
{
    public class SurveyController : ApiController
    {
        private readonly SurveyBO bo = new SurveyBO();

        public HttpResponseMessage PostSurvey(SurveyEntity survey)
        {
            survey = bo.AddSurvey(survey);
            HttpResponseMessage response;
            if (survey != null)
            {
                response = Request.CreateResponse<SurveyEntity>(HttpStatusCode.Created, survey);
                string uri = Url.Link("DefaultApi", new { id = survey.ID });

                response.Headers.Location = new Uri(uri);
            }
            else {

                response = Request.CreateResponse(HttpStatusCode.InternalServerError);
            }

            return response;
        }

        public List<SurveyEntity> GetAllSurveys()
        {
            return bo.GetAll();
        }
    }
}
