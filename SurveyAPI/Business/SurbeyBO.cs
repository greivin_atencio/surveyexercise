﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Survey.Model;
using Survey.DataAccess;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Data.Entity.Validation;

namespace SurveyAPI.Business
{
    public class SurveyBO
    {
        public SurveyEntity AddSurvey(SurveyEntity survey)
        {
            SurveyEntity result;
            survey.Sentiment = GetSentiment(survey.Comments);
            using (var db = new SurveyContext())
            {
                try
                {
                    result = db.Surveys.Add(survey);
                    db.SaveChanges();
                }
                catch (DbEntityValidationException ex) {
                    result = null;
                }
            }

            return result;
        }


        public List<SurveyEntity> GetAll()
        {
            List<SurveyEntity> result = null;
            using (var db = new SurveyContext())
            {
                result = db.Surveys.ToList();
            }

            return result;
        }


        private string GetSentiment(string comment)
        {

            string result = string.Empty;

            var client = new HttpClient();
            var queryString = HttpUtility.ParseQueryString(string.Empty);
            client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", "c5aaf7714d5a4574beb013a9b59e1aa0");

            var uri = "https://westus.api.cognitive.microsoft.com/text/analytics/v2.0/sentiment?" + queryString;

            HttpResponseMessage response;

            JObject document = new JObject
            {
                { "language", "en" },
                { "id", "1" },
                { "text", comment }
            };

            JArray documents = new JArray
            {
                document
            };

            JObject body = new JObject
            {
                ["documents"] = documents
            };
            // Request body
            byte[] byteData = Encoding.UTF8.GetBytes(body.ToString());

            using (var content = new ByteArrayContent(byteData))
            {
                content.Headers.ContentType = new MediaTypeHeaderValue("text/json");
                var postTask = client.PostAsync(uri, content);
                postTask.Wait();

                response = postTask.Result;
                if (response.IsSuccessStatusCode)
                {
                    JObject jsonResponse = JsonConvert.DeserializeObject<JObject>(response.Content.ReadAsStringAsync().Result);

                    double score = Double.Parse((string)jsonResponse["documents"][0]["score"]);

                    if (score < 0.5)
                    {
                        result = "Negative";
                    }
                    else if (score == 0.5)
                    {
                        result = "Neutral";
                    }
                    else if (score > 0.5)
                    {
                        result = "Positive";
                    }
                }
                else
                {
                    result = "There was an error with the sentiment API";
                }

            }
            return result;

        }
    }
}